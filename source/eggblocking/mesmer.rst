.. include:: /_references/mesmer_skills_reference.rst
.. include:: /_references/mesmer_traits_reference.rst

Mesmer / Chronomancer
=====================

Hi there o/. Here are methods I use to block things on the Triple Trouble event.
The main advantage of these methods is, that you only need to use one blocking skill per wurm action/spit.

.. contents:: Table of Contents
   :local:
   :depth: 2

.. _chrono_eggblock:

Egg + AoE Block
---------------

Things you want to equip
~~~~~~~~~~~~~~~~~~~~~~~~

Skills
''''''

* weapons: Sword/Shield and x/Focus
* heal: |Signet_of_the_Ether|_ [Signet of the Ether]
* utility: |Mimic|_ [Mimic]
* utility: |Signet_of_Midnight|_ [Signet of Midnight]
* utility: |Feedback|_ [Feedback]
* elite: |Timewarp|_ [Timewarp]

Traits
''''''

.. list-table::
   :widths: 50 25 25
   :header-rows: 1

   * - Trait Line
     - Important Traits
     - Things I use
   * - Domination
     - x-2-x
     - 2-2-2
   * - Inspiration
     - x-1-x
     - 1-1-2
   * - Chronomancer
     - x-2-2
     - 3-2-2

.. note:: If you do not have access to Chronomancer then use something else. Having or not having chrono isn't really important (for egg-only blocking).

|Domination|_ Domination:

* |Blurred_Inscriptions|_ [Blurred Inscriptions] (Major #2) grants distortion every time you activate a signet. You need this trait to block the AoE spits.

|Inspiration|_ Inspiration:

* |Inspiring_Distortion|_ [Inspiring Distortion] (Minor #2) grants distortion to allies whenever you get distortion. It's good to block AoE spits, because then only one of the blockers needs to stand on the sweet spot. Also helps other blockers if they would get hit by the spins.
* |Wardens_Feedback|_ [Warden's Feedback] (Major #2) allows you to block eggs with |Temporal_Curtain|_ [Focus #4]

|Chronomancer|_ Chronomancer:

* |Illusionary_Reversion|_ [Illusionary Reversion] (Major #2) helps to keep illusions alive when using |Distortion|_ [Distortion (F4)] or |Continuum_Split|_ [Continuum Split]

How to use the skills
~~~~~~~~~~~~~~~~~~~~~
Stay in the sweet spot at all times. Using the method from this guide you do not need to move away from it at any time.

.. seealso::  :ref:`sweet_spot_indicator`

Whenever the Wurm makes the spit animation, you place down your |Feedback|_ [Feedback]. If |Mimic|_ [Mimic] is up and you are not 100%
sure that the spit is an egg spit (e.g. after a burn phase), use |Mimic|_ [Mimic] before using |Feedback|_ [Feedback].

.. tip:: Place |Feedback|_ [Feedback] by clicking with the mouse on the skill. This puts it directly on your position.

To block AoE spits from small spins: use |Signet_of_Midnight|_ [Signet of Midnight] in the moment the wurm 'closes' its mouth and unwinds itself.

Sometimes the time between two small spins is not enough to recharge |Signet_of_Midnight|_ [Signet of Midnight], so you can use |Signet_of_the_Ether|_ [Signet of the Ether] to block the AoE.
Keep in mind though, that |Signet_of_the_Ether|_ [Signet of the Ether] has a cast time of 1 second, so you need to cast it a bit earlier.
A possible reference point here is when the wurm is curling up on the ground, just as it's about to open the mouth.

To block/'evade' large spins you have the choice between using |Echo_of_Memory|_ [Shield #4] or |Blurred_Frenzy|_ [Sword #2]. I recommend using |Echo_of_Memory|_ [Shield #4] whenever you can.

.. tip:: If you mess something up, you still have other stuff to help you out:

   * |Temporal_Curtain|_ [Focus #4] blocks eggs (place it like |Feedback|_ [Feedback] => click on the skill with the mouse)
   * |Distortion|_ [Distortion (F4)] grants you enough invulnerability to block a whole spit of eggs or husks - if you have 3 illusions up!

TL;DR
~~~~~

.. list-table::
   :widths: 20 80
   :header-rows: 1

   * - Wurm Action
     - What you need to use
   * - Large Spin
     - |Blurred_Frenzy|_ [Sword #2] or |Echo_of_Memory|_ [Shield #4]
   * - Small Spin + AoE Spit
     - |Signet_of_Midnight|_ [Signet of Midnight] or if not up: |Signet_of_the_Ether|_ [Signet of the Ether]
   * - Unknown Spit (Husks or Eggs possible)
     - |Mimic|_ [Mimic] => |Feedback|_ [Feedback]
   * - Known Egg Spit
     - |Feedback|_ [Feedback]
   * - Known Husk Spit
     - nothing
   * - Burn Phase
     - |Continuum_Split|_ [Continuum Split] => |Timewarp|_ [Timewarp]
   * - at any time
     - |Illusionary_Leap|_ [Sword #3] or |Phantasmal_Warden|_ [Focus #5] to ensure you have 3 illusions up


------------

.. _chrono_fullblock:

Fullblock (Egg, AoE, Husks)
---------------------------

Things you want to equip
~~~~~~~~~~~~~~~~~~~~~~~~

Skills
''''''

* weapons: Sword/Shield and x/Focus
* heal: |Signet_of_the_Ether|_ [Signet of the Ether]
* utility: |Signet_of_Illusions|_ [Signet of Illusions]
* utility: |Signet_of_Midnight|_ [Signet of Midnight]
* utility: |Feedback|_ [Feedback]
* elite: |Timewarp|_ [Timewarp]

.. note:: Like egg block but replace |Mimic|_ [Mimic] with |Signet_of_Illusions|_ [Signet of Illusions].

Traits
''''''

.. list-table::
   :widths: 50 25 25
   :header-rows: 1

   * - Trait Line
     - Important Traits
     - Things I use
   * - Domination
     - x-2-x
     - 2-2-2
   * - Inspiration
     - x-1-x
     - 1-1-2
   * - Chronomancer
     - x-2-2
     - 3-2-2

.. note:: Exactly like egg blocking. Only difference is, that having Chronomancer is now more or less required / optimal.

|Domination|_ Domination:

* |Blurred_Inscriptions|_ [Blurred Inscriptions] (Major #2) grants distortion every time you activate a signet. You need this trait to block the AoE spits.

|Inspiration|_ Inspiration:

* |Inspiring_Distortion|_ [Inspiring Distortion] (Minor #2) grants distortion to allies whenever you get distortion. It's good to block AoE spits, because then only one of the blockers needs to stand on the sweet spot. Also helps other blockers if they would get hit by the spins.
* |Wardens_Feedback|_ [Warden's Feedback] (Major #2) allows you to block eggs with |Temporal_Curtain|_ [Focus #4]

|Chronomancer|_ Chronomancer:

* |Illusionary_Reversion|_ [Illusionary Reversion] (Major #2) help to keep illusions alive when using |Distortion|_ [Distortion (F4)] or |Continuum_Split|_ [Continuum Split]


How to use the skills
~~~~~~~~~~~~~~~~~~~~~
Stay in the sweet spot at all times. Using the method from this guide you do not need to move away from it at any time.

.. seealso::  :ref:`sweet_spot_indicator`

Whenever the Wurm makes the spit animation and you do not know if it is a husk or an egg spit: use |Distortion|_ [Distortion (F4)].

* If it was a husk spit: use |Feedback|_ [Feedback] on next spit, because next spit will be an egg spit
* If it was an egg spit: use |Signet_of_Illusions|_ [Signet of Illusions] to recharge |Distortion|_ [Distortion (F4)] and ensure you have 3 illusions on the field (|Illusionary_Leap|_ [Sword #3], |Phantasmal_Warden|_ [Focus #5]). Next spit will be a husk spit, so use |Distortion|_ [Distortion (F4)] on it again.

.. seealso::  :ref:`indicate_husk_or_egg_spit`

To block AoE spits from small spins: use |Signet_of_Midnight|_ [Signet of Midnight] in the moment the wurm 'closes' its mouth and unwinds itself.

Sometimes the time between two small spins is not enough to recharge |Signet_of_Midnight|_ [Signet of Midnight], so you can use |Signet_of_the_Ether|_ [Signet of the Ether] to block the AoE.
Keep in mind though, that |Signet_of_the_Ether|_ [Signet of the Ether] has a cast time of 1 second, so you need to cast it a bit earlier.
A possible reference point here is when the wurm is curling up on the ground, just as it's about to open the mouth.

To block/'evade' large spins you have the choice between using |Echo_of_Memory|_ [Shield #4] or |Blurred_Frenzy|_ [Sword #2]. I recommend using |Echo_of_Memory|_ [Shield #4] whenever you can.

.. tip:: If you mess something up, you still have other stuff to help you out:

   * |Temporal_Curtain|_ [Focus #4] blocks eggs (place it like |Feedback|_ [Feedback] => click on the skill with the mouse)
   * Mask the cast time |Signet_of_Illusions|_ [Signet of Illusions] with invulnerability by casting |Signet_of_Midnight|_ [Signet of Midnight] during the cast of |Signet_of_Illusions|_ [Signet of Illusions]. This does not interrupt the existing cast, but applies instant invulnerability. This *really* helps if the wurm is doing an unknown or known husk spit and your |Distortion|_ [Distortion (F4)] is not up yet. This way you can recharge your |Distortion|_ [Distortion (F4)] and get the info if the spit is egg or husk (throug the number of floaty texts which appear because you blocked with the invulnerability from |Signet_of_Midnight|_ [Signet of Midnight]). Cast a reflect or |Distortion|_ [Distortion (F4)] afterwards, based on egg or husk spit.

Example Timeline
~~~~~~~~~~~~~~~~

.. list-table::
   :widths: 20 80
   :header-rows: 1

   * - Wurm Action
     - What you need to do
   * - spawns
     - find the sweet spot
   * - large spin
     - |Echo_of_Memory|_ [Shield #4] and |Illusionary_Leap|_ [Sword #3]
   * - large spin
     - |Echo_of_Memory|_ [Shield #4] again, you have now 3 illusions up
   * - unknown spit
     - |Distortion|_ [Distortion (F4)] => was egg spit, so use |Signet_of_Illusions|_ [Signet of Illusions]
   * - husk spit
     - |Distortion|_ [Distortion (F4)]
   * - pause
     -
   * - small spin
     - |Signet_of_Midnight|_ [Signet of Midnight]
   * - pause
     -
   * - burn phase
     - |Continuum_Split|_ [Continuum Split] => |Timewarp|_ [Timewarp], |Feedback|_ [Feedback] (optional, for Vet Jungle Wurms spit thingies)
   * - large spin
     - |Echo_of_Memory|_ [Shield #4], make sure you have 3 illusions up after the spin
   * - unknown spit
     - |Distortion|_ [Distortion (F4)] => was husk spit, no further actions needed
   * - egg spit
     - |Feedback|_ [Feedback]
   * - pause
     -


TL;DR
~~~~~

.. list-table::
   :widths: 20 80
   :header-rows: 1

   * - Wurm Action
     - What you need to use
   * - Large Spin
     - |Blurred_Frenzy|_ [Sword #2] or |Echo_of_Memory|_ [Shield #4]
   * - Small Spin + AoE Spit
     - |Signet_of_Midnight|_ [Signet of Midnight] or if not up: |Signet_of_the_Ether|_ [Signet of the Ether]
   * - Unknown Spit (Husks or Eggs possible)
     - |Distortion|_ [Distortion (F4)] with 3 illusions up
   * - Known Egg Spit
     - |Feedback|_ [Feedback]
   * - Known Husk Spit
     - |Distortion|_ [Distortion (F4)] with 3 illusions up, use |Signet_of_Illusions|_ [Signet of Illusions] before to recharge it if it is not up yet
   * - Burn Phase
     - |Continuum_Split|_ [Continuum Split] => |Timewarp|_ [Timewarp], |Feedback|_ [Feedback] (optional, for Vet Jungle Wurms spit thingies)
   * - at any time
     - |Illusionary_Leap|_ [Sword #3] or |Phantasmal_Warden|_ [Focus #5] to ensure you have 3 illusions up

.. tip:: This build also allows you to only eggblock.

   Use |Feedback|_ [Feedback] or |Temporal_Curtain|_ [Focus #4] on unknown spits or known egg spits and use |Distortion|_ [Distortion (F4)] if the last spit was husks and your |Feedback|_ [Feedback] is not yet off cooldown.