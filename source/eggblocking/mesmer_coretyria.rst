.. include:: /_references/mesmer_skills_reference.rst
.. include:: /_references/mesmer_traits_reference.rst

Mesmer (Core Tyria / No Elite Spec)
===================================

Heyo! These are alternative, but inferior builds to the chronomancer guides, in case you do not have access to it (yet).
These are not full guides, but only list the difference to the chronomancer guides (substitutions), so you will have to read the respective chrono guide as well.

Egg+AoE Block Alternative
-------------------------

Why bother, just drop the Chrono trait line and use the normal guide: :ref:`chrono_eggblock`

Fullblock Alternative
---------------------

Chrono Guide: :ref:`chrono_fullblock`

Things you want to equip
~~~~~~~~~~~~~~~~~~~~~~~~

Skills
''''''

* weapons: replace x/Shield with x/Sword so that you get Sword/Focus and x/Sword

Traits
''''''

.. list-table::
   :widths: 50 25 25
   :header-rows: 1

   * - Original Trait Line
     - Trait Line
     - Important Traits
   * - Chronomancer
     - whatever you want
     - x-x-x

TL;DR
~~~~~

.. list-table::
   :widths: 20 80
   :header-rows: 1

   * - Wurm Action
     - What you need to use
   * - Large Spin
     - |Blurred_Frenzy|_ [Sword #2] or |Illusionary_Riposte|_ [Sword #4]
   * - at any time
     - |Illusionary_Leap|_ [Sword #3] or |Phantasmal_Warden|_ [Focus #5] or [Sword #5] to ensure you have 3 illusions up

Blocking Large Spins
''''''''''''''''''''

[Sword #4] only blocks one attack, so don't block too early to avoid blocking veteran jungle wurms, crabs or risen attacks.
It'll also teleport you a bit away, so you need to move to the sweet spot again.
It does not offer any advantage over dodging the spin, since you need to move to the spot again in both cases.
Use |Blurred_Frenzy|_ [Sword #2] whenever possible (at least from my one test wurm it should always be up when the wurm does a large spin)


Double Unknown Spit
'''''''''''''''''''

In a scenario where two spits come after each other: first spit was eggs but you blocked with |Distortion|_ [Distortion (F4)], second spit is therefor husks.
You need to get three illusions up again and recharge your |Distortion|_ [Distortion (F4)] with |Signet_of_Illusions|_ [Signet of Illusions].

To create these illusions use |Illusionary_Leap|_ [Sword #3], then |Phantasmal_Warden|_ [Focus #5], weaponswap, [Sword #5].
In this build you do not have any other sources of illusions and using a shatter skill will destroy all of them.

Variants and Differences
''''''''''''''''''''''''

You could equip a scepter and use [Scepter #1] to spawn illusions but than you get into situations where the wurm does a wide spin and you are forced to dodge/block and teleport away from the sweet spot with [Scepter #2], having to relocate the spot afterwards.

Chronomancer traitline really helps with this: |Illusionary_Reversion|_ [Illusionary Reversion] & |Chronophantasma|_ [Chronophantasma] help to keep your illusions up.