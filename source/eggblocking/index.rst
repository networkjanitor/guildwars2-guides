.. include:: /_references/mesmer_skills_reference.rst
.. include:: /_references/mesmer_traits_reference.rst

Egg- and Fullblocking
=====================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   mesmer
   mesmer_coretyria


General Information
-------------------

.. _solo_vs_duo:

Solo vs. Duo Blocking
~~~~~~~~~~~~~~~~~~~~~

My opinion: Always block as if you are the only blocker on the wurm. There is no difference between 'solo' blocking and 'duo' blocking.
If the other blocker lags out or crashes then you are responsible that all the required spits are blocked.
Such things happen, more often than not without any indicator until it is too late.

OpenCommunity usually tries to always have two dedicated blockers per wurm, per run. If one fails, there is always a backup.

If you are fullblocking with a second person, you should communicate in chat what you have blocked (if you got the 'Invulnerable!' floaty texts).
Place an 'e' into party chat if you have blocked eggs or a 'h' if you have blocked husks.

.. _spit_timers:

Spit Timers
~~~~~~~~~~~

Egg spits are possible around every 45 seconds, while husk spits are possible around every 90 seconds.
Usually both kind of spits are possible after a burn phase, so be wary of that.

.. _indicate_husk_or_egg_spit:

How to tell if it was a husk or an egg spit
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Blocked with Invulnerability
''''''''''''''''''''''''''''

If you blocked a spit with invulnerability (like |Distortion|_ [Distortion]) you will get a different number of floaty texts,
depending on the kind of spit you blocked. If you blocked a husk, you'll see a single 'Invulnerable!' floaty text.
If you blocked a set of eggs, you'll see three 'Invulnerable!' floaty texts. These floaty texts can appear 3 times per spit,
so you'll get a total of 3 'Invulnerable!' when blocking a full husk spit or 9 'Invulnerable!' when blocking a full egg spit.

Blocked AoE spits will generate a large amount of 'Invulnerable!' floaty texts.

.. figure:: /_static/block_husk.png

   Screenshot of the 'Invulnerable!' floaty text because of a blocked husk

.. note:: Visual representation of egg spit floaties soon available.


.. figure:: /_static/block_aoespit.png

   Screenshot of the 'Invulnerable!' floaty texts because of a blocked AoE spits during a small spin

Only one person will get to see the floaty texts.

.. seealso:: :ref:`solo_vs_duo`

Blocked with Reflects
'''''''''''''''''''''

If you blocked a spit with reflects (like |Feedback|_ [Feedback] or traited |Temporal_Curtain|_ [Temporal Curtain])
you will not get any combat-log messages or floaty texts. However, if nothing landed in the arena and nobody is fullblocking, then it was an egg spit.

.. _sweet_spot_indicator:

How to find the sweet spot
~~~~~~~~~~~~~~~~~~~~~~~~~~

The sweet spot is the location on which you need to position yourself if you plan on blocking things with invulnerability (like husks or AoE spits).
It is more or less in the center of the wurm.

.. figure:: /_static/sweet_spot.png
   :figwidth: 75%

   Screenshot of the sweet spot on the crimson wurm

In order to find the sweet spot you need to hit the wurm with a melee of your choice - after it is targetable.
What you'll see is a tiny thing pop up (in the middle of the wurm) for every hit you land on the wurm. Go to the spot of the thingie - that's the sweet spot.

.. figure:: /_static/sweet_spot_animation.png

   Screenshot of the sweet spot hit animation using mesmer sword #1. With other professions the animation might differ.
   Screenshot was take on Amber Wurm while the wurm was buried (after swallowing)


