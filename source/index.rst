.. guildwars2-guides documentation master file, created by
   sphinx-quickstart on Mon Jul 17 07:11:36 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

GuildWars2 Guides and Stuff
===========================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   eggblocking/index
   huskhandling/index
   commander-stuff/index



Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
