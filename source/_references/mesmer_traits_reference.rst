.. |Domination| image:: /_static/Domination.png
   :height: 24px
   :width: 24px

.. _Domination: https://wiki.guildwars2.com/wiki/Domination

.. |Inspiration| image:: /_static/Inspiration.png
   :height: 24px
   :width: 24px

.. _Inspiration: https://wiki.guildwars2.com/wiki/Inspiration

.. |Chronomancer| image:: /_static/Chronomancer.png
   :height: 24px
   :width: 24px

.. _Chronomancer: https://wiki.guildwars2.com/wiki/Chronomancer

.. |Blurred_Inscriptions| image:: /_static/Blurred_Inscriptions.png
   :height: 24px
   :width: 24px

.. _Blurred_Inscriptions: https://wiki.guildwars2.com/wiki/Blurred_Inscriptions

.. |Inspiring_Distortion| image:: /_static/Inspiring_Distortion.png
   :height: 24px
   :width: 24px

.. _Inspiring_Distortion: https://wiki.guildwars2.com/wiki/Inspiring_Distortion

.. |Wardens_Feedback| image:: /_static/Wardens_Feedback.png
   :height: 24px
   :width: 24px

.. _Wardens_Feedback: https://wiki.guildwars2.com/wiki/Warden's_Feedback

.. |Illusionary_Reversion| image:: /_static/Illusionary_Reversion.png
   :height: 24px
   :width: 24px

.. _Illusionary_Reversion: https://wiki.guildwars2.com/wiki/Illusionary_Reversion

.. |Chronophantasma| image:: /_static/Chronophantasma.png
   :height: 24px
   :width: 24px

.. _Chronophantasma: https://wiki.guildwars2.com/wiki/Chronophantasma