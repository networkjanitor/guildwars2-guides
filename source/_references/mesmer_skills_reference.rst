.. |Timewarp| image:: /_static/Time_Warp.png
   :height: 24px
   :width: 24px

.. _Timewarp: https://wiki.guildwars2.com/wiki/Time_Warp

.. |Signet_of_the_Ether| image:: /_static/Signet_of_the_Ether.png
   :height: 24px
   :width: 24px

.. _Signet_of_the_Ether: https://wiki.guildwars2.com/wiki/Signet_of_the_Ether

.. |Signet_of_Midnight| image:: /_static/Signet_of_Midnight.png
   :height: 24px
   :width: 24px

.. _Signet_of_Midnight: https://wiki.guildwars2.com/wiki/Signet_of_Midnight

.. |Signet_of_Illusions| image:: /_static/Signet_of_Illusions.png
   :height: 24px
   :width: 24px

.. _Signet_of_Illusions: https://wiki.guildwars2.com/wiki/Signet_of_Illusions

.. |Mimic| image:: /_static/Mimic.png
   :height: 24px
   :width: 24px

.. _Mimic: https://wiki.guildwars2.com/wiki/Mimic

.. |Feedback| image:: /_static/Feedback.png
   :height: 24px
   :width: 24px

.. _Feedback: https://wiki.guildwars2.com/wiki/Feedback

.. |Continuum_Split| image:: /_static/Continuum_Split.png
   :height: 24px
   :width: 24px

.. _Continuum_Split: https://wiki.guildwars2.com/wiki/Continuum_Split

.. |Blurred_Frenzy| image:: /_static/Blurred_Frenzy.png
   :height: 24px
   :width: 24px

.. _Blurred_Frenzy: https://wiki.guildwars2.com/wiki/Blurred_Frenzy

.. |Illusionary_Leap| image:: /_static/Illusionary_Leap.png
   :height: 24px
   :width: 24px

.. _Illusionary_Leap: https://wiki.guildwars2.com/wiki/Illusionary_Leap

.. |Temporal_Curtain| image:: /_static/Temporal_Curtain.png
   :height: 24px
   :width: 24px

.. _Temporal_Curtain: https://wiki.guildwars2.com/wiki/Temporal_Curtain

.. |Phantasmal_Warden| image:: /_static/Phantasmal_Warden.png
   :height: 24px
   :width: 24px

.. _Phantasmal_Warden: https://wiki.guildwars2.com/wiki/Phantasmal_Warden

.. |Echo_of_Memory| image:: /_static/Echo_of_Memory.png
   :height: 24px
   :width: 24px

.. _Echo_of_Memory: https://wiki.guildwars2.com/wiki/Echo_of_Memory

.. |Distortion| image:: /_static/Distortion.png
   :height: 24px
   :width: 24px

.. _Distortion: https://wiki.guildwars2.com/wiki/Distortion


