OpenCommunity TT: Behind the Scenes
===================================

Or as it might be called: *Commander Training Advice*.

.. note::   This version is considered draft quality - basically unpolished notes from the Triple
            Trouble Commander Training on 2017-08-16. Feel free to create pull requests and issues on
            https://gitlab.com/networkjanitor/guildwars2-guides or message me suggestion in game at xyoz.6710.

.. note::   This version was not updated since I wrote it. I know for a fact that some things are now handled
            differently at OpenCommunity, for better or worse. I'm still going to leave this up, because it is still
            a nice view behind the scenes and still gives a good impression on the organizational and technical side of
            the event. It does NOT give a good impression on the "social" side of commanding where you are supposed to
            engage with players through conversations at any cost (including making up opinions), trying to make them
            feel like heroes and overall make them feel good.

.. contents:: Table of Contents
   :local:
   :depth: 2

Checklist: Things that need to be done before and after a wurm
--------------------------------------------------------------

.. list-table::
   :widths: 50 50
   :header-rows: 1

   * - Time
     - Thing to do
   * - a day before
     - check if enough commanders are available for the next day, if not: poke people!
   * - 50 min before
     - start map spamming
   * - 20 min before, after map spamming
     - whisper OC guilds (mostly DV, KIT) and ping discord
   * - 5 min before
     - start looking for a map, explain that we are looking for a map
   * - on event start
     - publish taxi, announce how the taxi thing works
   * - after getting on the map
     - paste the two messages (who we are and ts, etc)
   * -
     - decide who commands which wurm
   * -
     - tell people to stack up, explain /supplyinfo
   * - XX:40
     - start going down to the subchannels
   * - <event>
     - <do the thing>
   * - after event as FSV
     - paste the two messages (thanks for joining, who we are and what we do)
   * -
     - announce and explain links in teamspeak
   * -
     - talk about wurms
   * -
     - give feedback, enter result of kill in spreadsheet



Mapspam & Gathering
-------------------

* visit every map
* paste creative message in map chat, informing the players that Triple Trouble is happening

    * do not use absolute times ("21:20 CEST"), use relative times ("in 30 minutes") instead, to
      avoid confusion with timezones

* note the ip of every spammed map in a shared spreadsheet, so that a map doesn't get spammed twice
* commanders might spam the community guilds (KIT, DV, etc) as well
* non-commanders are to be advised to invite friends and people from their guilds, random people next
  to them

    * they should not be spamming maps to avoid spamming a map twice

* time to start mapspamming: 20:30 CEST / 50 mins before gathering time
* this (like everything else) is a team effort, you are not required to do this on every wurm
  by yourself. split the tasks between you and the other commanders
* when mapspamming, there should always be at least one commander present in the Triple Trouble channel
  on teamspeak

    * allows that upcoming questions can be answered there
    * shows that the run is really happening / adds credibility to the run

Finding a map (instance)
------------------------

* commanders try to find a suitable instance on their own
* to avoid getting placed in a filled instance you should perform the following steps before trying
  finding a map:

    * do not represent any guild
    * be not in a squad or party
    * go offline :sup:`[todo]: verify` & restart the gw2 client

* go to Firthside Vigil Waypoint ([&BKoBAAA=]) and count the number of players there

    * if there are too many players >15 :sup:`[todo]: verify` then the instance is to be regarded as unsuited

* shared spreadsheet between gw2 triple trouble organizing communities to reserve maps and not get
  disputes about who is allowed to organize triple trouble on that map

    * first come, first serve: who enters the ip first gets to organize triple trouble on that map
    * might also help to find a community to merge with, if numbers are really low (unable to do all
      3 wurms)
    * merge is more likely on reset-wurms (3:00 CEST) and very unlikely on evening times

* time to start finding a map: 21:15 CEST / 5 mins before gathering time

Taxi
----

* commanders create a tag-less squad on which people can join and then transfer themselves in the
  selected instance
* the taxisquad should not have a tag to avoid confusion, especially once the three wurm tags/squads
  are up
* to create a tag-less squad you need 2 people:

    * person 1 creates a squad
    * person 2 joins the squad
    * the creator (person 1) leaves the squad, the squad is now tag/comm-less
    * the name of person 2 is posted in teamspeak, this persons squad/person is known as the taxi

* you should set a squadmessage, explaining what the squad is used for and how to join another person
  map instance

.. seealso:: See :ref:`xutt` for useful message presets/suggestions during the organization of a triple
             trouble event.

* each Bloodtide Coast instance is limited to approximately 150 players
* if the map fills up very fast and many players from teamspeak cannot get into the map, commanders
  probably will have to try to find another instance. This might be related to a bad choice in the
  selection of the instance or may indicate a "bugged" map, which caps at ~75 players (very rare)
  :sup:`[todo]: verify`
* as a taxi you are responsible for telling newly joined players on teamspeak the squadjoin/taxi
  (e.g.: "Taxi: /sqjoin <yourname>")
* as a taxi you do not join any wurm until the dry runs start

    * you can however already decide on a wurm beforehand and tell the commander about that (might even
      stack with them)



Deciding who commands which wurm
--------------------------------

* I was told people scream at each other and it's a blood bath
* after it's decided the commanders move to their positions and tag up:

    * north of the waypoint / cobalt / blue tag
    * east of the waypoint / amber / purple tag
    * south of the waypoint / crimson / red tag

Stacking & Counting
-------------------

* during the time between tagging up and going into the dry runs commanders advice their players to
  join a squad of their choice and stack right below the tag
* commanders count the stack using the /supplyinfo command

.. figure:: /_static/supplyinfo.png

    Example Output of the /supplyinfo command

* as a commander this is the one of the first chances to find out how well your squad is listening to
  you and following your orders
* stacking and standing still should bring people into an attentive mood

    * that's important, because you as a commander want your squad to do what you want

* people not listening and therefore not stacking may indicate that they will behave the same way
  during the run

    * plan larger margins for the mechanics (e.g. on amber: instead of 20+7 people you might need
      20+12 people to reliable trigger a burn phase)
    * instruct players to join on teamspeak (might not be there and then not knowing that they should
      stack)

* wanted numbers per subsquad:

    * Subsquad 1 (main mechanic): 25-30 players
    * Subsquad 2 (blockers/diboofs): 1-2 players
    * Subsquad 3 (husk handlers/huskies): 3-4 players

* if the numbers look not promising, open up lfg

    * open lfg in the 30 minutes before the event starts
    * close lfg as soon as the dry runs start
    * discuss with the other commanders who/how many should open lfg

Listening Count
~~~~~~~~~~~~~~~

* before the dry run start, you can do a short count on how many people are listening and following
  your orders in this very moment:

    #. place at least 2 location markers in front of you and tell your squad on teamspeak to go to the one
       you want them to go
    #. after ~10 seconds move yourself to the marker as well and use the /supplyinfo command to count the
       players

* there are multiple variations possible, e.g. place 2 markers but move yourself to the other marker
  instead of not moving

Dry Run
-------

* should start with a basic, overview-ish explanation of what will happen

    * should not include much detail
    * should bring players to listen up now / grab their attention
    * may include the "warning" that all the wurms need to be decapitated within 1 minute, else the
      event might fail

* should mention the meaning of typical gw2 phrases because especially new players are very likely to
  be unfamiliar with them:

    * cc / crowd control :sup:`[todo]: add usable explanation`
    * stacking on tag :sup:`[todo]: add usable explanation`

Amber Wurm Specials
-------------------

* use the /supplyinfo command after you and your squad go swallowed to determine if a burn phase is
  likely

    * threshold: around 28 players
    * you probably want to do this after every swallow, not just during the last one. This gives you a
      general feeling of how well your squad performs.

.. tip:: Using the Arrow-Up key inside the writing part of the chat window allows you to select earlier
         submitted texts. This way you do not need to type /supplyinfo each time. You could copy & paste
         the command as well.

* use the safe way on the right side of the wurm after you got spat out to get to the wurm and do not
  run directly into the wurm

    * this way it's unlikely that harpoon shots collide with other foes instead of the wurm

* amber is the wurm which will initiate the decapitation phase (1 minute timer)
* when all wurms are ready, initiate the final abomination kill and count the players inside the wurms
  stomach again
* if you are confident that you have enough players (=> harpoons) inside, inform the other commanders
  about this
* once informed, they will initiate the burn phase on their own wurms
* if you aren't sure that you have enough harpoons you should tell them this as well.
  This way they can wait until you initiate the burn phase to perform their mechanic/burn phase as well.

* if the timer reaches 1:40/1:30, you need to initiate the last abomination kill - even if the other
  wurms are not ready yet.

Extended Burn Phase
~~~~~~~~~~~~~~~~~~~

Once the 20 stack of Slick Skin are removed from the wurm (through harpoons), the wurm gets vulnerable.
The internal timer however, which determines when the wurm gets invlunerable again, doesn't start until
the current playing animation is completed. To get the maximum amount of extra-vulnerable-time, you
should try to remove the last stack of Slick Skin right as the wurm starts spitting eggs/husks.
The spit animation the longest animation and also doesn't require your squad to dodge anything
(unlike the spins).

Two Squad Tactic
~~~~~~~~~~~~~~~~

* expert tactic / not suited for daily use
* have two squads à ~30 players
* the two squads alternate between getting swallowed and waiting for the Upset Stomach debuff to disappear
* this allows for a much faster wurm kill but requires way more coordination and more players than usual

Crimson Wurm Specials
---------------------

* while running the colors you might want to talk about where the color clouds are and the current
  status of the extractors
* it's acceptable to only "oversee" the color filling process and running in circles - without actually
  focusing on filling the colors yourself
* Helpsquads / Support from and to Crimson Wurm:

    * it's not desirable to send your squad to help on other wurms (time to run back is too long /
      no near wp)
    * other squads can help you more easily however, because they can wp back once they are done helping
    * running from amber to crimson takes around the same amount of time as porting to Firthside
      Vigil Waypoint and running from there to crimson

* to prepare for the decapitation you should fill the red and yellow extractor - because the blue
  one is the easiest to fill
* once amber squad is initiating the last abomination kill you can instruct your squad to find and
  pick up the blue color buff (but don't fill yet)
* fill the last (blue) extractor once amber is confident that they will decapitate within the time span
* if the timer reaches 1:00 you need to initiate the filling of the last extractor - even if the other
  wurms are not ready yet

Portal Tactic
~~~~~~~~~~~~~

* expert tactic / not suited for daily use

Color buffs disappear when using a portal. However you can portal from the wurm/extractor to the color
cloud and then run back to the extractor. This allows for a faster filling of the extractors - but
requires more coordination and discipline.

Compared to Cobalt the portal tactic doesn't shine much here, because you need to fill three different
extractor and the color clouds are not always on the same positions. A color cloud also only holds
20 color buff charges - an extractor requires 30 however [correct me if I'm wrong, please - I'm not
sure on this one].

Cobalt Wurm Specials
--------------------

* keg spots change on every even timer minute (12:00, 10:00, 8:00, 6:00, 4:00 and 2:00)
* if you are not confident enough that you can get a burn phase of the jumping puzzle location, then you
  might want to not even try and clear the arena instead

    * this gets more relevant when preparing for decapitation - you might want to wait for a different
      keg location

* if the timer reaches 1:10 - 0:50 you need to initiate the last keg run - even
  if the other wurms are not ready yet:

    * 1:10 for jumping puzzle
    * 1:00 for mast
    * 0:50 for beach
    * remember to add the time required to reach the keg spot if you are not on the keg spot yet

Portal Tactic
~~~~~~~~~~~~~

During the burn phase let mesmers prepare portal(s) to portal the squad from inside the wurm to the keg
location - this way they only need to run the distance once (from keg spot to wurm). Portals should be
up as soon as the burn phase ends. As always: while this allows for a much faster wurm kill, it requires
way more coordination and is therefore not suited for daily use.

Other Stuff
-----------

Burn Phase Goals
~~~~~~~~~~~~~~~~

* you should aim for at least 15% damage to the wurm per burn phase
* explain might stacking and combo fields / burn phase stuff in say chat if the 15% dmg goal is not
  reached

Disconnects
~~~~~~~~~~~

* if you have an unreliable internet connection and/or are likely to crash/disconnect during the event
  you can ask one of the other team members (commanders, trainees, lieutenants) to be your backup (shadow)
* they will replace you until you come back and "calm" the squad during this time (in case you disconnect)

Wurm Attacks
~~~~~~~~~~~~

* wurm attacks / animations are listed here: https://www.youtube.com/watch?v=L7ATGPl4Itw
* you should always try to mention what the wurm currently does and what action the squad should take
* e.g. dodge on spin, avoiding husk landing spots on spit, avoiding orange circles on drool spit
* mention that the squad should dodge _before_ they actually need to dodge (also applies to phase 2
  [charge, flop])
* experienced commanders usually know what the wurm is doing by listening to the distinctive sounds the
  wurm makes

Commander Requirements
~~~~~~~~~~~~~~~~~~~~~~

* basically none, a level 56 (Bloodtide Coast map-level) character is all you need
* you'll get told everything (=> knowledge) during your time as trainee
* before you get accepted into the triple trouble team you will have a voice chat with Wolfie
  (the TT team manager) therefore having a microphone and being willing to try to talk is required
* :sup:`[todo] not sure about that one, can only tell from observations` while not being forced to talk
  while not commanding, you are expected to talk when you command.

Husk Spots & other location markers
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* the marker locations for the husk spots are usually learned from experience
* sometimes the team will train placing the markers and remembering their locations after a run
* only a minority of commanders use tools like `TacO <https://gw2taco.blogspot.de/>`_ to show them
  the location of where the markers should be

.. tip:: TODO: create page with screenshots and descriptions of husk landing spots

Whisperlist
~~~~~~~~~~~

* teamspeak allows you to talk to persons outside of your current channel - this feature is called 'whisper'
* you can whisper to a single person or even to a group, such as the servergroup 'Whisperlist' on the
  OpenCommunity teamspeak
* commanders use the whisperlist during the triple trouble event to coordinate the wurm decap (1 min timer)
  as well as general organization
* the whisper(-list) options can be found in teamspeak under Tools -> Options -> Whisper -> Whisperlist
* this feature is currently missing from discord :sup:`[todo] verify` which is one of the reasons why
  OpenCommunity uses teamspeak for event organization instead of discord

Learning the Commander Stuff
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you made it this far down, you are probably interested in becoming a Commander for OpenCommunitys Triple
Trouble team. While you'll porbably be told all this stuff during your time as Trainee, I want to list the
other ways to acquire knowledge during your Trainee and Commander time as well:

* Learning by doing: actually commanding events/wurms is a big part in becoming a Commander. You'll learn
  from your own experiences and might even try out new things and see how they work out for you.
* Learning from other Commanders, Trainees or Lieutenant: You'll learn from their experiences as they tell
  you the way they are commanding and handling things. It's important to pass knowledge from the older and
  experienced commanders to the new and upcoming commanders.
* Learning from a Mentor: while not the usual way, you can request for a mentor to be assigned to you.
  Your mentor will then work closely together with you and train you in commanding the event in a more
  direct, 1 on 1 manner. Both of you want to command as many wurms as possible - together.
  :sup:`[todo] Lelling mentioned this by the way, not sure if this is whats actually happening - needs work!`
* Learning from outside of the team - from other staff members. To combat knowledge loss over time, staff
  members from other teams (Raids, Event, Admins) might bring old and forgotten knowledge back into the
  Triple Trouble team, because they themselves commanded Triple Trouble a long time ago.
