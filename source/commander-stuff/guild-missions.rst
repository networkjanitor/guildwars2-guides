Commanding PvE Guild Missions
=============================

Hello. Commanding guild missions is some high premium privilege. They are the hardest and most difficult content in the
entire game. Jokes aside, having some pointers, links and race paths at hand is pretty useful and makes for smooth
guild missions.

If you want to read detailed stuff about guild missions, check out `<http://dulfy.net/2013/02/26/gw2-guild-missions-guide/>`_.
This here are merely pointers and summaries.

Puzzles
-------

very puzzeling!

.. tip:: Never, ever respawn at last checkpoint. Always wait for people to pick you up, otherwise you get stuck and have to redo events alone.

Angvar's Trove
``````````````

.. note:: Waypoint: Scholar's Cleft Waypoint [&BLcAAAA=], Snowden Drifts.

Part 1: get jumping!
''''''''''''''''''''

I like left way. If you fall down, wait below the platform - portals will be provided.

Part 2: Split into two groups.
''''''''''''''''''''''''''''''

Top Group: Pick up bows and shoot the small icicles. Don't shoot the big one in the middle.

Bottom Group: Pick up the icicles and plug the steam vents with them. Put in the middle two last.

When the first two platforms are up, shoot the big icicle. It's only going to last for around a minute.

Repeat process for last two platforms. Once down, light all torches at the same time.

Part 3: more jumping!
'''''''''''''''''''''

Don't fall down. Afterwards climb back up, and get your chest on the ship. Wait for everyone before proceeding.

Part 4: barrel time!
''''''''''''''''''''

Pick up barrels. Each ice wall needs 1 barrel to be destroyed, place it with skill #1. Last wall takes 3, you can use skill #2 for that.

Part 5: more jumping?
'''''''''''''''''''''

Take shortcut jump with swiftness or dodge jump.

Part 6: get torched!
''''''''''''''''''''

One person picks up a cooled egg and brings with it the drake guardian to each of the shrines. Everyone else picks up a
torch and lights it at the lit up shrines. We need 6 torches in total, one of each shrine.

Proxemics Lab
`````````````

.. note:: Waypoint: Gallowfields Waypoint [&BGMAAAA=], Brisban Wildlands.

Part 1: shiny!
''''''''''''''

Open the door by finding batteries in the six junk piles. Interact with them until you get a lightning sparkle thingie
in your hands. Bring that to the middle of the room.

Part 2: jumping!
''''''''''''''''

Don't fall down and if you do, stay down - portals will be provided.

Part 3: match animals!
''''''''''''''''''''''

Transform into animals and then walk into the matching chambers to power the elevator. Three times.

Part 4: jumping again.
''''''''''''''''''''''

Wait until everyone is gathered up, then press the button. At the end 4 players need to stand on the buttons to open the door.

Part 5: aMAZEing.
'''''''''''''''''

Find orbs in maze, climb up stairs to get a better view of the maze. Get all six orbs and bring them to the end of the maze.
Some of the walls are destructible and the traps can be deactivated from the four platforms after climbing up the stairs.

At the end get your cheese triangle.

Langmar Estate
``````````````

.. note:: Waypoint: Vir's Gate Waypoint — [&BH8BAAA=], Plains of Ashford.

Part 1: sword, staff, bow!
''''''''''''''''''''''''''

Match weapons from humans and charrs so that the charrs win.

.. list-table::
   :widths: 50 50
   :header-rows: 1

   * - Charr
     - Human
   * - Bow
     - Sword
   * - Sword
     - Staff
   * - Staff
     - Bow

Interact with the weapon racks to find the weapons. If an ascalonian ghost spawns, ignore the rack
(because only ghosts will spawn there) and carry on.

Part 2: Stop having the gate be closed!
'''''''''''''''''''''''''''''''''''''''

2 people to the left, 2 people to the right. The rest gathers in front of the gate.

Part 3: 3,2,1, pull!
''''''''''''''''''''

6 people to the outer curtains, 6 people stay at the inner curtains.

If you are at the inner curtains, you will have to pull it open on my command and then whisper the emote you can see to
the person standing at the respective outer curtain. When you pull the curtain, make sure you don't accidentally speak to the npc.

If you are the outer curtains you will then have to perform that emote. For the outer curtains, put an x in chat.



.. tip::   Annonce the pairs and who whispers who.


.. note::    It can only be one of the following emotes: /dance, /sit, /point, /salute, /bow, /wave

Pull in 3, 2, 1, now!

Part 4: build ram!
''''''''''''''''''

2 people stay on the first level, stand on the buttons. This will remove the posion gas from the next level.

2 people stay on the second level and keep killing the oozes to grease the rusty gears with the Balls of Ooze.

Everyone else goes all the way down. Interact with the broken carts to gather materials to build the ram.

Once the ram is build interact with it once, smash open the door and then accept credit.

Challenges
----------

Blightwater Shatterstrike
`````````````````````````

.. note:: Blightwater Shatterstrike @ Tumok's Waypoint [&BPsBAAA=], Blazeridge Steppes.

Two groups, one left one right.

Wait at the start to get your weapons. Then start damaging the nodes on your side until around 20%.
Once all nodes are at 20% we can start killing them. All nodes need to be killed withing 30 seconds, otherwise they respawn.

Branded For Termination
```````````````````````

.. note:: Branded For Termination @ Helliot Mine Waypoint [&BEsBAAA=], Fields of Ruin.

Three groups, one left, one right, one down.

Damage your champion to 15% and then wait until everyone is ready. All champions need to be killed within 30 seconds.

Deep Trouble
````````````

.. note:: Deep Trouble @ Irwin Isle Waypoint — [&BNICAAA=], Mount Maelstrom.

3 or more groups depending on amount of players. Aim for groups of 3-5.

Free quaggans and then escort them safely to their destination. Each group only frees one quaggan at a time.

Protect your quaggan by killing the nearby krait, if it dies we fail.

Save Our Supplies
`````````````````

.. note:: Save Our Supplies @ Viper's Run Waypoint — [&BOoBAAA=], Iron Marches.

Spread out around the camp. Kill stuffs. Keep an eye on the camp to spot enemies that managed to get through.
Don't touch the turrets. Stalker spawn at around 8 minutes remaining. Igniters spawn at around 4 minutes remaining.

Scratch Sentry Defense
``````````````````````

.. note:: Scratch Sentry Defense @ Stromkarl Waypoint — [&BEYEAAA=], Timberline Falls.

Three groups, one top left for jotuns, one top right for spiders and one down for ettins. If any of the skritt die, we fail the mission.


Southsun Crab Toss
``````````````````

.. note:: Southsun Crab Toss @ Lion Point Waypoint — [&BNAGAAA=], Southsun Cove.

One person takes the crab, healers and a couple more people defend the crab carrier while they are running circles inside
the event area. As a crab carrier you shouldn't get too close to the red ring, otherwise it counts as leaving the area and failing the mission.

Everyone else follow them as well but prioritizes the veteran and champion karkas spawning later on. Protect the crab carrier
at all costs, if they get downed we fail the mission.


Races
-----

.. note:: Make sure everyone is on the same map when they sign up for the race.

.. tip::

     Representing the "Bank" guild and activating the mission from that guild's missions tab, talk to the NPC at the
     end of the race and accept credit. So long as the timer has not run out and 15/15 players have finished the race,
     the "Bank" guild will receive guild completion rewards.

Bear Lope
`````````

.. note:: Bear Lope @ Durmand Priory Waypoint — [&BOkAAAA=], Lonar's Pass.

Chicken Run
```````````

.. note:: Chicken Run @ Fangfury Watch Waypoint — [&BEwBAAA=], Fields of Ruin.

Crab Scuttle
````````````

.. note:: Crab Scuttle @ Lion Point Waypoint — [&BNAGAAA=], Southsun Cove.

Devourer Burrow
```````````````

.. note:: Devourer Burrow @ Manbane's Waypoint — [&BMkDAAA=], Diessa Plateau.

Ghost Wolf Run
``````````````

.. note:: Ghost Wolf Run @ Faun's Waypoint — [&BKUAAAA=], Harathi Hinterlands.

Quaggan Paddle
``````````````

.. note:: Quaggan Paddle @ Twoloop Waypoint — [&BIACAAA=], Frostgorge Sound.

Spider Scurry
`````````````

.. note:: Spider Scurry @ Hessdallen Kenning Waypoint — [&BF0CAAA=], Dredgehaunt Cliffs.



