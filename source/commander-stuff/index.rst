Commander Stuff
===============

Hi there o/. In addition to the here presented guides I also created some tools for commanding events in GuildWars2.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   octt-behind-the-scenes
   guild-missions

.. _xutt:

Xutt
----

Link: https://networkjanitor.gitlab.io/xutt/

Xutt is a collection of datasets, which contain useful explanation messages about different meta/world boss events.
These messages are supposed to be posted into the squad chat during the event explanation (dry run/escort) or during the actual event (broadcasts).

In the here linked html version, these messages can be copied into the clipboard on the click of a button.
A native linux client with hotkey support is also available: https://gitlab.com/networkjanitor/bronk

The datasets itself is available in this repository: https://gitlab.com/networkjanitor/xutt

