Husk Handling
=============

.. contents:: Table of Contents
   :local:
   :depth: 2

Husk Spots
----------

Crimson
~~~~~~~

First Spot
''''''''''

.. figure:: /_static/husk_spots/crimson_1_a.jpg
   :figwidth: 100%

   Perspective A on the first husk spot in the crimson arena.

.. figure:: /_static/husk_spots/crimson_1_b.jpg
   :figwidth: 100%

   Perspective B on the first husk spot in the crimson arena.

Second Spot
'''''''''''

.. figure:: /_static/husk_spots/crimson_2_a.jpg
   :figwidth: 100%

   Perspective A on the second husk spot in the crimson arena.

.. figure:: /_static/husk_spots/crimson_2_b.jpg
   :figwidth: 100%

   Perspective B on the second husk spot in the crimson arena.


Third Spot
''''''''''

.. figure:: /_static/husk_spots/crimson_3_a.jpg
   :figwidth: 100%

   Perspective A on the third husk spot in the crimson arena.

.. figure:: /_static/husk_spots/crimson_3_b.jpg
   :figwidth: 100%

   Perspective B on the third husk spot in the crimson arena.

Amber
~~~~~

First Spot
''''''''''

.. figure:: /_static/husk_spots/amber_1_a.jpg
   :figwidth: 100%

   Perspective A on the first husk spot in the amber arena.

.. figure:: /_static/husk_spots/amber_1_b.jpg
   :figwidth: 100%

   Perspective B on the first husk spot in the amber arena.

Second Spot
'''''''''''

.. figure:: /_static/husk_spots/amber_2_a.jpg
   :figwidth: 100%

   Perspective A on the second husk spot in the amber arena.

.. figure:: /_static/husk_spots/amber_2_b.jpg
   :figwidth: 100%

   Perspective B on the second husk spot in the amber arena.


Third Spot
''''''''''

.. figure:: /_static/husk_spots/amber_3_a.jpg
   :figwidth: 100%

   Perspective A on the third husk spot in the amber arena.

.. figure:: /_static/husk_spots/amber_3_b.jpg
   :figwidth: 100%

   Perspective B on the third husk spot in the amber arena.

Cobalt
~~~~~~

First Spot
''''''''''

.. figure:: /_static/husk_spots/cobalt_1_a.jpg
   :figwidth: 100%

   Perspective A on the first husk spot in the cobalt arena.

.. figure:: /_static/husk_spots/cobalt_1_b.jpg
   :figwidth: 100%

   Perspective B on the first husk spot in the cobalt arena.

Second Spot
'''''''''''

.. figure:: /_static/husk_spots/cobalt_2_a.jpg
   :figwidth: 100%

   Perspective A on the second husk spot in the cobalt arena.

.. figure:: /_static/husk_spots/cobalt_2_b.jpg
   :figwidth: 100%

   Perspective B on the second husk spot in the cobalt arena.


Third Spot
''''''''''

.. figure:: /_static/husk_spots/cobalt_3_a.jpg
   :figwidth: 100%

   Perspective A on the third husk spot in the cobalt arena.

.. figure:: /_static/husk_spots/cobalt_3_b.jpg
   :figwidth: 100%

   Perspective B on the third husk spot in the cobalt arena.

